
fn _test(input: char, existing_value: u32) -> u32 {
    let x = input as u32 % 32; 
    println!("value of x {}", x);
    let value: u32 = 0x1 << input as u32 % 32;
    println!("value of value {}", value);
    existing_value ^ value;

    let res = existing_value.count_ones() as u32;
    res
}


fn window_test(input: &[u8]) {
    input.iter().take(14-1);
    input.windows(14).position(|w| {
        println!("{:?}", w);
        let res = true;
        res
    });
    true
}


fn check_for_duplicate(input: &str) -> u32 {
    let mut filter = 1u32;
    for c in input.to_lowercase().chars() {
        let current_value = filter;
        let value = 0x1 << c as u32 % 32; 
        filter ^= value;
        if filter < current_value {
            println!("Duplicate found: {}", c);
            return value;
        }
    }
    println!("Only unique letters in string");
    filter
}



fn main() {
    let next = check_for_duplicate("abcdefghijklmnopqrstuvwxyz");
    println!("value of next : {:b}", next);

    let ascii_string = "abcdefghiklmnopqrstuvwxyz";
    window_test(ascii_string.as_bytes());

}

